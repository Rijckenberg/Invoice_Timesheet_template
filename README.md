# Personaldata_Invoice_Timesheet_Excel_template
Integrated Invoice and Timesheet Excel template for European I.T. Freelancers, including 

   - automated invoice calculation based on timesheet data and data in "personal_data" tab
   
   - separate Excel tab - called "Personal_Data" - containing consultant company, intermediary company and client company data .

Currency used in template is the euro (€).

If file conversion is required from XLS file format to PDF file format:

1) first convert XLS to PDF using WPS Office  in Windows/Linux
2) merge PDFs using PDFSam Basic in Windows/Linux
3) Optional: sign PDF using Adobe Fill & Sign on iPad
